## Freshcaller-Agile Integration

## Installation guidelines

1. Your Freshcaller Subdomain :
   example :- In "www.xyz.freshcaller.com" xyz is subdomain.

2. Your Agile Crm Email Address :
   Enter your email address registered with AgileCRM.

3. Agile CRM API key
   Steps :-
   a. Log into Agile CRM with your registered email address
   b. Go to admin settings
   c. Go to Developers & API section
   d. copy the 𝘙𝘌𝘚𝘛 𝘈𝘗𝘐 key.

### Folder structure explained

    .
    ├── README.md                  This file
    ├── app                        Contains the files that are required for the front end component of the app
    │   ├── app.js                 JS to render the dynamic portions of the app
    │   ├── icon.svg               Sidebar icon SVG file. Should have a resolution of 64x64px.
    │   ├── freshcaller_logo.png   The Freshcaller logo that is displayed in the app
    │   ├── style.css              Style sheet for the app
    │   ├── template.html          Contains the HTML required for the app’s UI
    ├── config                     Contains the installation parameters and OAuth configuration
    │   ├── iparams.json           Contains the parameters that will be collected during installation
    │   └── iparam_test_data.json  Contains sample Iparam values that will used during testing
    └── manifest.json              Contains app meta data and configuration information
