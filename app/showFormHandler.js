/**This function renders the form handler when the user clicks on add to contacts button */
function showFormHandler() {
    document.getElementById('buttonID').style.display = 'none';
    document.getElementById('contactInfoId').style.display = 'none';
    document.getElementById('formBodyId').style.display = 'block';
    document.getElementById('circularG').style.display = 'none';
}

/**This function renders te button when there is no contact found in the CRM */
function showButton(phoneNum) {
    document.getElementById('buttonID').style.display = 'flex';
    document.getElementById('contactInfoId').style.display = 'block';
    document.getElementById('phoneId').value = phoneNum;
}

/**This function is invoked when the agent submits the form and checks for the mandatory fields */
function formSubmitHandler(event) {
    event.preventDefault();

    /* disable button and enable loader in button */
    document.getElementById('formSubmitId').disabled = true;
    document.getElementById('formSubmitId').style.cursor = 'not-allowed';
    document.getElementById('submitTextId').style.display = 'none';
    document.getElementById('circularG').style.display = 'block';

    let firstName = document.getElementById('fname').value;
    let lastName = document.getElementById('lname').value;
    let email = document.getElementById('email').value;
    let phoneNum = document.getElementById('phoneId').value;
    let postData = `{
                  "properties": [
                      {
                          "type": "SYSTEM",
                          "name": "first_name",
                          "value": "${firstName}"
                      },
                      {
                          "type": "SYSTEM",
                          "name": "last_name",
                          "value": "${lastName}"
                      },
                      {
                          "type": "SYSTEM",
                          "name": "email",
                          "subtype": "work",
                          "value": "${email}"
                      },
                      {
                          "name": "phone",
                          "value": "${phoneNum}",
                          "subtype": "work"
                      }
                    ]
                 }`;
    const newHeaders = {
        Accept: 'application/json',
        Authorization: "Basic <%= encode(iparam.email + ':' + iparam.apiKey)%>",
        'Content-Type': 'application/json'
    };
    const newOptions = { headers: newHeaders, body: postData };
    const url = `https://<%=iparam.subdomain%>.agilecrm.com/dev/api/contacts`;
    client.request
        .post(url, newOptions)
        .then(function(resData) {
            if (resData.status === 200) {
                /* hide the form */
                document.getElementById('formBodyId').style.display = 'none';

                /* show the success message and form data */
                document.getElementById('newContactMessageId').style.display = 'block';
                let updatedObj = JSON.parse(resData.response);
                updatedObj.properties.forEach(function(element) {
                    if (element.name === 'first_name') {
                        document.getElementById('subjectFirstName').innerText = element.value;
                    }
                    if (element.name === 'last_name') {
                        document.getElementById('subjectLastName').innerText = element.value;
                    }
                    if (element.name === 'email') {
                        document.getElementById('subjectEmailId').innerText = element.value;
                    }
                    if (element.name === 'phone') {
                        document.getElementById('subjectPhoneNo').innerText = element.value;
                    }
                });
            }
        })
        .catch(function(error) {
            /* show error message and make email id field red when error found */
            if (error.status === 400) {
                document.getElementById('email').style.borderBottom = '1px solid red';
                document.getElementById('labelEmailId').style.color = 'red';
                document.getElementById('formErrorId').innerText = 'Email Id already exists';
                document.getElementById('formSubmitId').disabled = false;
                document.getElementById('circularG').style.display = 'none';
                document.getElementById('submitTextId').style.display = 'block';
                document.getElementById('formSubmitId').style.cursor = 'pointer';
            } else if (error.status === 406) {
                document.getElementById('email').style.borderBottom = '1px solid red';
                document.getElementById('labelEmailId').style.color = 'red';
                document.getElementById('formErrorId').innerText = "Can't add more contacts";
                document.getElementById('formSubmitId').disabled = false;
                document.getElementById('circularG').style.display = 'none';
                document.getElementById('submitTextId').style.display = 'block';
                document.getElementById('formSubmitId').style.cursor = 'pointer';
            } else {
                console.error('Error occured while fetching data', JSON.stringify(error));
                document.getElementById('formBodyId').style.display = 'none';
                let errorOccured = errorRender();
                $('#errorOccuredId').append(errorOccured);
            }
        });
}
