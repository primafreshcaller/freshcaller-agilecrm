const headers = { Accept: 'application/json', Authorization: "Basic <%= encode(iparam.email + ':' + iparam.apiKey)%>" };
const options = { headers: headers };
let client = '';
$(document).ready(function() {
    app.initialized()
        .then(function(_client) {
            client = _client;
            let appActivatedPromise = notificationAppActivated();
            appActivatedPromise
                .then(function() {
                    console.log('Completed');
                })
                .catch(function(e) {
                    console.error('Error occured while Activating', JSON.stringify(e));
                    let errorOccuredText = errorRender();
                    $('#errorOccuredId').append(errorOccuredText);
                });
        })
        .catch(function(e) {
            console.error('Error occured while initialising the document', JSON.stringify(e));
            let errorOccuredText = errorRender();
            $('#errorOccuredId').append(errorOccuredText);
        });
});

function notificationAppActivated() {
    return new Promise(function(resolve, reject) {
        client.events.on('app.activated', function() {
            let subdomainPromise = getSubdomain();
            subdomainPromise
                .then(function(subdomain) {
                    let notificationDetailsPromise = notficationDetailsFetch(subdomain);
                    notificationDetailsPromise
                        .then(function() {
                            return resolve();
                        })
                        .catch(function(e) {
                            console.error('Error occured while fetching notification details', JSON.stringify(e));
                            return reject(e);
                        });
                })
                .catch(function(e) {
                    console.error('Error occured while fetching Subdomain', JSON.stringify(e));
                    return reject(e);
                });
        });
    });
}
/**This function fetches details for notification area*/

function notficationDetailsFetch(subdomain) {
    return new Promise(function(resolve, reject) {
        /**Fetch Customer details From Agile CRM*/
        const customerDetailsPromise = getCustomerDetails(subdomain);
        customerDetailsPromise
            .then(function(customerDetails) {
                /**If Contact is present in AGILE CRM */
                if (customerDetails.length != 0) {
                    let fullName = fetchContactName(customerDetails);
                    let customerContactId = customerDetails[0].id;
                    let nameRenderNotification = notificationNameRender(subdomain, fullName, customerContactId);
                    $('#customerNameNotification').append(nameRenderNotification);
                    let customerDealPromise = fetchCustomerDealDetails(customerDetails, subdomain);
                    customerDealPromise
                        .then(function(dealsDetailsArray) {
                            let latestDeal = dealsDetailsArray[0];
                            let dealIDStyleNotification = notificationDealRender(latestDeal, subdomain);
                            $('#customerDealNotification').append(dealIDStyleNotification);
                        })
                        .catch(function(e) {
                            console.error('Error occured while fetching Deal Details', JSON.stringify(e));
                        });
                    return resolve();
                    /**If Contact is not present in AGILE CRM */
                } else {
                    let noDataString = 'No Contact Found!';
                    $('#noInfoId').append(noDataString);
                    return resolve();
                }
            })
            .catch(function(e) {
                console.error('Contact Details Promise did not resolve', JSON.stringify(e));
                return reject(e);
            });
    });
}
