/**API HELPERS */
/**This function returns the company Name of the customer*/
function fetchCompanyName(customerDetails) {
    let customerProperties = customerDetails[0].properties;
    let companyName = customerProperties.filter(function(property) {
        return property.name === 'company';
    });
    if (companyName.length != 0) {
        return companyName[0].value;
    }
    return '';
}
/**This function returns the deal details of the customer*/

function fetchCustomerDealDetails(customerDetails, subdomain) {
    return new Promise(function(resolve, reject) {
        let customerContactId = customerDetails[0].id;
        let urlFetch = `https://${subdomain}.agilecrm.com/dev/api/contacts/${customerContactId}/deals`;
        client.request
            .get(urlFetch, options)
            .then(function(dealsDetails) {
                if (dealsDetails.response) {
                    let dealsDetailsArray = JSON.parse(dealsDetails.response);
                    return resolve(dealsDetailsArray);
                }
            })
            .catch(function(e) {
                console.error('Error occured while fetching DealID', JSON.stringify(e));
                return reject(e);
            });
    });
}
/**This function returns the name of the customer from the property array */

function fetchContactName(customerDetails) {
    let customerProperties = customerDetails[0].properties;
    let firstName = '',
        lastName = '';
    customerProperties.forEach(function(customerProperty) {
        if (customerProperty.name === 'first_name') {
            firstName = customerProperty.value;
        } else if (customerProperty.name === 'last_name') {
            lastName = customerProperty.value;
        }
    });
    return lastName ? `${firstName} ${lastName}` : firstName;
}
/**This function returns the Promise with customer details if present or a Promise with
 * an empty customerDetails Array if there is no csutomer details
 */
function getCustomerDetails(subdomain) {
    return new Promise(function(resolve, reject) {
        let phoneNumPromise = getPhoneNumber();
        phoneNumPromise
            .then(function(phoneNum) {
                let url = `https://${subdomain}.agilecrm.com/dev/api/search?q=${phoneNum}&page_size=10&type="PERSON"`;
                client.request
                    .get(url, options)
                    .then(function(customerDetailsRes) {
                        if (customerDetailsRes.response) {
                            let customerDetails = JSON.parse(customerDetailsRes.response);
                            return resolve(customerDetails);
                        }
                    })
                    .catch(function(e) {
                        console.error('Error occured while fetching Contact Details : ', JSON.stringify(e));
                        return reject(e);
                    });
            })
            .catch(function(err) {
                console.error('Error occured while resolving PhoneNumber Promise', JSON.stringify(err));
                return reject(err);
            });
    });
}
/**This function returns the Promsie subdomain from the saved iparams */
function getSubdomain() {
    return new Promise(function(resolve, reject) {
        client.iparams
            .get()
            .then(function(iparams) {
                let subdomain = iparams.subdomain;
                return resolve(subdomain);
            })
            .catch(function(e) {
                console.error('Error Occured while fetching Installation properties', JSON.stringify(e));
                return reject(e);
            });
    });
}
/** This function returns the Promise  phoneNumber for an incoming call , using callDetails.caller.from
 * or
 * returns the phoneNumber of the dialled number for an outgoing call using callDetails.caller.to
 */
function getPhoneNumber() {
    return new Promise(function(resolve, reject) {
        client.data
            .get('currentCall')
            .then(function(callDetails) {
                if (callDetails.caller) {
                    if (callDetails.caller.from) {
                        let phoneNum = callDetails.caller.from;
                        return resolve(phoneNum);
                    } else {
                        let phoneNum = callDetails.caller.to;
                        return resolve(phoneNum);
                    }
                }
            })
            .catch(function(err) {
                console.error('Error occured while fetching phone number of the caller ', JSON.stringify(err));
                return reject(err);
            });
    });
}
/**HTML RENDER HELPERS */
function activityLinkRender(subdomain) {
    const activityLink = `https://${subdomain}.agilecrm.com/#activities`;
    let activityLinkStyle = ` <a  class="activityTextLinkClass" href=${activityLink} target ="_blank"> Go to activity page </a>`;
    return activityLinkStyle;
}
function calendarRender(subdomain) {
    const calendarLink = `https://${subdomain}.agilecrm.com/#calendar`;
    let calendarLinkStyle = ` <a class="calendarTextLinkClass" href=${calendarLink} target ="_blank"> Add Calendar </a>`;
    return calendarLinkStyle;
}
function inConversationNameRender(subdomain, customerContactId, fullName) {
    let url = `https://${subdomain}.agilecrm.com/#contact/${customerContactId}`;
    let nameRender = `<div class = "headerName">
                Contact name:
                <div class = "fullName">
                <a class="nameLinkClass" href =${url} target ="_blank">
                ${fullName}
                </a>
                </div>
                </div>                    
                `;
    return nameRender;
}
function inConversationDealRender(subdomain, latestDeal) {
    const url = `https://${subdomain}.agilecrm.com/#deal/${latestDeal.id}`;
    let dealIDStyle = `
                    <div class = "latestDealHeader">
                    Recent Lead ID/Deal ID/Account id:
                    <div class = "dealIdLatest">
                    <a class="dealLinkClass" href = ${url} target ="_blank">
                    ${latestDeal.id} 
                    </a>
                    </div>
                    </div>
                    `;
    return dealIDStyle;
}
function inConversationDealValueRender(latestDealValue) {
    let dealValueStyle = `
                            <div class = "dealValueHeader">
                            Potential deal size :
                            <div class = "dealValue">
                            ${latestDealValue}
                            </div>
                            </div>
                            `;
    return dealValueStyle;
}
function inConversationCompanyNameRender(companyName) {
    let companyNameStyle = `<div class = "headerCompanyName">
                            Company name:
                            <div class = "companyName">
                            ${companyName}
                            </div>
                            </div>                    
                            `;
    return companyNameStyle;
}
function notificationNameRender(subdomain, fullName, customerContactId) {
    let url = `https://${subdomain}.agilecrm.com/#contact/${customerContactId}`;
    let nameRenderNotification = `<div class = "headerName">
                                Contact name:
                                <div class = "fullName">
                                <a class="nameLinkClassNotification" href =${url} target ="_blank">
                                ${fullName}
                                </a>
                                </div>
                                </div>                    
                                `;
    return nameRenderNotification;
}
function notificationDealRender(latestDeal, subdomain) {
    const url = `https://${subdomain}.agilecrm.com/#deal/${latestDeal.id}`;
    let dealIDStyleNotification = `
                                <div class = "latestDealHeader">
                                Recent Lead ID/Deal ID/Account id:
                                <div class = "dealIdLatest">
                                <a class="dealLinkClassNotification" href = ${url} target ="_blank">
                                ${latestDeal.id} 
                                </a>
                                </div>
                                </div>
                                `;
    return dealIDStyleNotification;
}
/**ERROR RENDER HELPERS */
function errorRender() {
    let text = 'Oops! Some error occurred. Please try after sometime or contact tech support';
    let errorOccuredText = `<div class ="errorMessageBody">${text}</div>`;
    return errorOccuredText;
}
