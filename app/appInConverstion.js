const headers = { Accept: 'application/json', Authorization: "Basic <%= encode(iparam.email + ':' + iparam.apiKey)%>" };
const options = { headers: headers };
let client = '';
$(document).ready(function() {
    app.initialized()
        .then(function(_client) {
            client = _client;
            let subdomainPromise = getSubdomain();
            subdomainPromise
                .then(function(subdomain) {
                    let inConversationDetailsPromise = inConversationDetailsFetch(subdomain);
                    inConversationDetailsPromise
                        .then(function() {
                            console.log('Completed');
                        })
                        .catch(function(e) {
                            console.error('Error occured while fetching notification details', JSON.stringify(e));
                            let errorOccuredText = errorRender();
                            $('#errorOccuredId').append(errorOccuredText);
                        });
                })
                .catch(function(e) {
                    console.error('Error occured while fetching Subdomain', JSON.stringify(e));
                    let errorOccuredText = errorRender();
                    $('#errorOccuredId').append(errorOccuredText);
                });
        })
        .catch(function(e) {
            console.error('Error occured while initialising the document', JSON.stringify(e));
            let errorOccuredText = errorRender();
            $('#errorOccuredId').append(errorOccuredText);
        });
});

function inConversationDetailsFetch(subdomain) {
    return new Promise(function(resolve, reject) {
        const customerDetailsPromise = getCustomerDetails(subdomain);
        customerDetailsPromise
            .then(function(customerDetails) {
                if (customerDetails.length != 0) {
                    let activityLinkStyle = activityLinkRender(subdomain);
                    $('#activityLink').append(activityLinkStyle);
                    let calendarLinkStyle = calendarRender(subdomain);
                    $('#calendarLink').append(calendarLinkStyle);
                    let fullName = fetchContactName(customerDetails);
                    let customerContactId = customerDetails[0].id;
                    let nameRender = inConversationNameRender(subdomain, customerContactId, fullName);
                    $('#customerName').append(nameRender);
                    let customerDealPromise = fetchCustomerDealDetails(customerDetails, subdomain);
                    let companyName = fetchCompanyName(customerDetails);
                    if (companyName) {
                        let companyNameStyle = inConversationCompanyNameRender(companyName);
                        $('#companyName').append(companyNameStyle);
                    } else {
                        let companyNameStyle = inConversationCompanyNameRender('No Company Details');
                        $('#companyName').append(companyNameStyle);
                    }
                    customerDealPromise
                        .then(function(dealsDetailsArray) {
                            let latestDeal = dealsDetailsArray[0];
                            let dealIDStyle = inConversationDealRender(subdomain, latestDeal);
                            $('#customerDeal').append(dealIDStyle);
                            let latestDealValue = parseInt(dealsDetailsArray[0].expected_value);
                            let dealValueStyle = inConversationDealValueRender(latestDealValue);
                            $('#dealValue').append(dealValueStyle);
                        })
                        .catch(function(e) {
                            console.error('Error occured while fetching Deal Details', JSON.stringify(e));
                        });
                    return resolve();
                } else {
                    getPhoneNumber()
                        .then(function(phoneNum) {
                            showButton(phoneNum);
                            return resolve();
                        })
                        .catch(function(e) {
                            console.error('Error occured while fetching phoneNum', JSON.stringify(e));
                            return reject(e);
                        });
                }
            })
            .catch(function(e) {
                console.error('Contact Details Promise did not resolve', JSON.stringify(e));
                return reject(e);
            });
    });
}
